from pantarei import *

def f(x):
    return x

task = Task(f, cache=Cache('/tmp/data'))
job = Job(task, cluster=Cluster(), cores=2)
result = job(x=1)
print(result)
result = job(x=2)
print(result)
