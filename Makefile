# Fill in your package name and path (identical to PROJECT if there are not subpackages)
PACKAGE = pantarei

.PHONY: install test coverage docs pep8 clean

all: install

install:
	pip install .

test:
	python -m unittest tests.test tests.test_db

coverage:
	coverage run -m unittest tests.test tests.test_db
	coverage report --omit=pantarei/cli.py --omit=tests/*py

pep8:
	autopep8 -r -i $(PACKAGE)
	flake8 $(PACKAGE)

docs:
	emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=docs/tutorial.org -f org-rst-export-to-rst --kill
	# pdoc -o docs/api --force --html --skip-errors $(PACKAGE)
	orgnb docs/*org
	make -C docs/ html

clean:
	find $(PROJECT) tests -name '__pycache__' -name '*.pyc' -exec rm '{}' +
	rm -rf build/ dist/


