{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "89f08c4e",
   "metadata": {},
   "source": [
    "This tutorial is also available as\n",
    "[jupyter](https://framagit.org/coslo/pantarei/-/blob/master/docs/tutorial.ipynb)\n",
    "and\n",
    "[org-mode](https://framagit.org/coslo/pantarei/-/blob/master/docs/tutorial.org)\n",
    "notebook.\n",
    "\n",
    "# Quick start\n",
    "\n",
    "[Pantarei](https://framagit.org/coslo/pantarei) is a simple,\n",
    "general-purpose data and workflow manager. It draws a lot of inspiration\n",
    "from [joblib](https://joblib.readthedocs.io/), which is a very nice\n",
    "package, but has some **limitations**:\n",
    "\n",
    "-   **cosmetic changes** to code triggers re-execution\n",
    "-   no easy way to **browse** the cache and collect **datasets**\n",
    "-   no *native* support for **distributed computing**\n",
    "\n",
    "Pantarei was developed to address these issues. It builds on three kinds\n",
    "of execution units:\n",
    "\n",
    "-   **functions**: stateless, Python callables\n",
    "-   **tasks**: stateful wrapped functions that cache execution results\n",
    "-   **jobs**: stateful wrapped tasks for distributed-memory parallel\n",
    "    environments\n",
    "\n",
    "## Caching function execution\n",
    "\n",
    "Let\\'s start with a simple Python function\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58ac6ca1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "\n",
    "def square(x):\n",
    "    \"\"\"Compute the square of `x`\"\"\"\n",
    "    time.sleep(2)\n",
    "    return x**2"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "807a9649",
   "metadata": {},
   "source": [
    "Pretty much like joblib, `pantarei` caches the execution of\n",
    "functions, but using *classes* instead of *decorators* to do the\n",
    "wrapping. Here, we wrap the `square` function with a\n",
    "`Task` and call it with a range of arguments\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f9a1e67c",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pantarei import Task\n",
    "\n",
    "task = Task(square)\n",
    "for x in [1, 2, 3]:\n",
    "    print(task(x=x))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "e35b5fc6",
   "metadata": {},
   "source": [
    "The task\\'s results are cached: a successive execution will just fetch\n",
    "the results\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "321dc248",
   "metadata": {},
   "outputs": [],
   "source": [
    "task(x=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "905bde00",
   "metadata": {},
   "source": [
    "```\n",
    "What if you edit now the function =square=? Compare with the joblib behavior!\n",
    "```"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "f39f9d18",
   "metadata": {},
   "source": [
    "To always clear the cache of a `Task` before execution, use\n",
    "the `clear_first` argument of the `Task`\n",
    "constructor (this is useful when debugging a code). To fully remove the\n",
    "cache of a task, for all combinations of parameters, use\n",
    "`task.clear_all()`.\n",
    "\n",
    "## Embarassing parallelism\n",
    "\n",
    "With `pantarei`, it is easy to run tasks for different\n",
    "combinations of input parameters on a distributed computing environment\n",
    "like an HPC cluster. This allows your code to run in parallel over many\n",
    "more cores than on a single shared-memory environment of your processor.\n",
    "\n",
    "To achieve this, you need a scheduler (ex. SLURM) installed on the\n",
    "machine you are running your code. If you have access to an HPC cluster,\n",
    "execute your workflow over there: `pantarei` will auto-detect\n",
    "the scheduler. Otherwise, no problem! You can install a fallback\n",
    "scheduler on your machine by downloading\n",
    "[nohupx](https://framagit.org/coslo/nohupx), a minimal scheduler written\n",
    "in bash, and putting it somewhere in your `$PATH`.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f6fb437",
   "metadata": {},
   "source": [
    "wget https://framagit.org/coslo/nohupx/-/raw/master/nohupx\n",
    "mv nohupx env/bin/\n",
    "chmod u+x env/bin/nohupx"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "e756b061",
   "metadata": {},
   "source": [
    "We can now wrap the task with a `Job` and submit jobs to the\n",
    "scheduler\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab1187b1",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pantarei import Job\n",
    "\n",
    "job = Job(Task(square))\n",
    "for x in [4, 5, 6]:\n",
    "    job(x=x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8ef1dd6",
   "metadata": {},
   "source": [
    "```\n",
    "Two current limitations of pantarei =Jobs= are: (i) the name of Job instances must be =job= and (ii) all arguments to =job= must be passed by keyword. Hopefully, they will be removed soon.\n",
    "```"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "8839634d",
   "metadata": {},
   "source": [
    "The jobs will be running in the background. To see a summary of the jobs\n",
    "in the current session, execute this or add it at the end of the script.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2cd7bcd9",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pantarei.core import report\n",
    "\n",
    "report()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "c212dee3",
   "metadata": {},
   "source": [
    "In this case, the jobs were already over! You can also see the status of\n",
    "the jobs using the scheduler from the command line. For instance, with\n",
    "`nohupx` you can check the jobs\\' queue like this\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "751153a2",
   "metadata": {},
   "source": [
    "nohupx queue"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "00bfac7f",
   "metadata": {},
   "source": [
    "Once the jobs are done, we can get the actual results.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db4db2d0",
   "metadata": {},
   "outputs": [],
   "source": [
    "job.scheduler.wait()\n",
    "results = job(x=6)\n",
    "print(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "85cc2047",
   "metadata": {},
   "source": [
    "```\n",
    "Jobs and tasks share the same cache. Therefore, calling a =Job= that wraps a =Task= that has been already called will not trigger a resubmission.\n",
    "```"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "c7ee8658",
   "metadata": {},
   "source": [
    "## Command line interface\n",
    "\n",
    "Suppose we collect the above commands in a script, say\n",
    "`script.py`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41130244",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pantarei import *\n",
    "\n",
    "def f(x):\n",
    "    import time\n",
    "    time.sleep(2)\n",
    "    return x\n",
    "\n",
    "task = Task(f)\n",
    "for x in [1, 2]:\n",
    "    task(x=x)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "d10063d8",
   "metadata": {},
   "source": [
    "From the command line, you can execute the script and check the state of\n",
    "the jobs by changing the execution mode like this\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5b944119",
   "metadata": {},
   "outputs": [],
   "source": [
    "! rei run script.py  # run the full script"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "71568356",
   "metadata": {},
   "outputs": [],
   "source": [
    "! rei summary script.py  # summary of the jobs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f03f6e37",
   "metadata": {},
   "outputs": [],
   "source": [
    "! rei run --timid script.py  # go through the script without running the tasks/jobs"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "4885744c",
   "metadata": {},
   "source": [
    "# Workflow and data management\n",
    "\n",
    "## A minimal simulation workflow\n",
    "\n",
    "Let\\'s build a minimal simulation workflow comprising the following\n",
    "steps:\n",
    "\n",
    "-   **production**: run a simulation using a `run()` driver,\n",
    "    which returns some data\n",
    "-   **analysis**: postprocess the data and compute some stats using a\n",
    "    `postprocess()` function\n",
    "-   **plot**: plot some graphs using the above data with some\n",
    "    `plot()` function\n",
    "\n",
    "We emulate this setup with these functions\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4103d38",
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "import numpy\n",
    "\n",
    "def run(T=1.0, n=12, seed=1):\n",
    "    # Emulate some long running simulation...\n",
    "    time.sleep(2)\n",
    "    # Return some synthetic thermodynamic data\n",
    "    numpy.random.seed(seed)\n",
    "    raw = numpy.random.normal(size=1000)\n",
    "    data = {'U': 3/2 * T * raw, 'W': T + raw * n/3}\n",
    "    return data\n",
    "\n",
    "def postprocess(data):\n",
    "    results = {}\n",
    "    # Compute stats\n",
    "    for key in data:\n",
    "        results[f'mean_{key}'] = numpy.mean(data[key])\n",
    "        results[f'std_{key}'] = numpy.std(data[key])\n",
    "    # Compute virial-potential energy correlation\n",
    "    R = numpy.corrcoef(data['U'], data['W'])[0, 1]\n",
    "    results['corr_UW'] = R\n",
    "    return results"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "eda564d7",
   "metadata": {},
   "source": [
    "Let\\'s turn each step into a task and run the workflow\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7faf2b87",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pprint import pprint\n",
    "\n",
    "data = Task(run)()\n",
    "results = Task(postprocess)(data)\n",
    "pprint(results)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "616c3e7e",
   "metadata": {},
   "source": [
    "## Browsing datasets\n",
    "\n",
    "The tasks arguments and results are stored in cache in a way similar to\n",
    "joblib. However, pantarei provides eacy access to the **tasks\\'\n",
    "datasets** through dedicated `Dataset` and `Data`\n",
    "objects.\n",
    "\n",
    "Let\\'s first make some simulations for a range of parameters\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9163db53",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pantarei import Task\n",
    "\n",
    "run = Task(run)\n",
    "pp = Task(postprocess)\n",
    "\n",
    "for T in [1.0, 2.0, 3.0]:\n",
    "    for seed in range(2):\n",
    "        data = run(T=T, seed=seed)\n",
    "        pp(data)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "257d1cc8",
   "metadata": {},
   "source": [
    "We can now browse all the arguments and results as a \\\"dataset\\\"\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "77cac4ce",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pantarei.core import browse\n",
    "\n",
    "browse('postprocess')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "6c2ab190",
   "metadata": {},
   "source": [
    "The returned dataset is a `Dataset` object with\n",
    "`pandas`-like features\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6400262a",
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = browse('run')\n",
    "print(dataset.columns())\n",
    "print(dataset['T'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62a5c28c",
   "metadata": {},
   "source": [
    "```\n",
    "You can always inspect the cache by looking inside the =.pantarei/= folder. Clearing the cache \"manually\" is fine, too.\n",
    "```"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "ebe08b8e",
   "metadata": {},
   "source": [
    "We can create a `Dataset` explictly by providing paths and\n",
    "parsers\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c9171497",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pantarei import Dataset\n",
    "\n",
    "dataset = Dataset(parsers=[('pickle', '*.pkl')])\n",
    "dataset.insert('.pantarei/run/*/*')\n",
    "dataset"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "648b3be6",
   "metadata": {},
   "source": [
    "Actually, `browse` parses any files in tabular, yaml or\n",
    "pickle format to populate the dataset. If your simulation code store\n",
    "artifacts in subfolders, you can qucikly create and analyze them using\n",
    "Datasets!\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b438557",
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    import time\n",
    "    time.sleep(2)\n",
    "    return x"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "cc0a9880",
   "metadata": {},
   "source": [
    "Wrap the function with a Task and call it with a range of arguments\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2434bd8e",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pantarei import *\n",
    "\n",
    "task = Task(f)\n",
    "for x in [1, 2]:\n",
    "    task(x=x)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "1749e96e",
   "metadata": {},
   "source": [
    "The task\\'s results are cached: a successive execution will just fetch\n",
    "the results\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5cf235fc",
   "metadata": {},
   "outputs": [],
   "source": [
    "task(x=1)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "7da08a9a",
   "metadata": {},
   "source": [
    "We wrap the task with a Job and submit jobs to a local scheduler (like\n",
    "SLURM)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "777742ca",
   "metadata": {},
   "outputs": [],
   "source": [
    "job = Job(task)\n",
    "for x in [3, 4]:\n",
    "    job(x=x)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "3fe908fa",
   "metadata": {},
   "source": [
    "Once the jobs are done, we can get the results (which are cached too)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c6d2a8c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "job.scheduler.wait()\n",
    "results = job(x=3)"
   ]
  }
 ],
 "metadata": {},
 "nbformat": 4,
 "nbformat_minor": 5
}
