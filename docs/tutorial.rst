========
Tutorial
========


This tutorial is also available as `jupyter <https://framagit.org/coslo/pantarei/-/blob/master/docs/tutorial.ipynb>`_ and `org-mode <https://framagit.org/coslo/pantarei/-/blob/master/docs/tutorial.org>`_ notebook.

Quick start
-----------

`Pantarei <https://framagit.org/coslo/pantarei>`_ is a simple, general-purpose data and workflow manager. It draws a lot of inspiration from `joblib <https://joblib.readthedocs.io/>`_, which is a very nice package, but has some **limitations**:

- **cosmetic changes** to code triggers re-execution

- no easy way to **browse** the cache and collect **datasets**

- no *native* support for **distributed computing**

Pantarei was developed to address these issues. It builds on three kinds of execution units:

- **functions**: stateless, Python callables

- **tasks**: stateful wrapped functions that cache execution results

- **jobs**: stateful wrapped tasks for distributed-memory parallel environments

Caching function execution
~~~~~~~~~~~~~~~~~~~~~~~~~~

Let's start with a simple Python function

.. code:: python

    import time

    def square(x):
        """Compute the square of `x`"""
        time.sleep(2)
        return x**2

Pretty much like joblib, ``pantarei`` caches the execution of functions, but using *classes* instead of *decorators* to do the wrapping. Here, we wrap the ``square`` function with a ``Task`` and call it with a range of arguments

.. code:: python

    from pantarei import Task

    task = Task(square)
    for x in [1, 2, 3]:
        print(task(x=x))

::

    1
    4
    9


The task's results are cached: a successive execution will just fetch the results

.. code:: python

    task(x=1)

::

    1


.. admonition:: Question

    What if you edit now the function ``square``? Compare with the joblib behavior!

To always clear the cache of a ``Task`` before execution, use the ``clear_first`` argument of the ``Task`` constructor (this is useful when debugging a code). To fully remove the cache of a task, for all combinations of parameters, use ``task.clear_all()``.

Embarassing parallelism
~~~~~~~~~~~~~~~~~~~~~~~

With ``pantarei``, it is easy to run tasks for different combinations of input parameters on a distributed computing environment like an HPC cluster. This allows your code to run in parallel over many more cores than on a single shared-memory environment of your processor.

To achieve this, you need a scheduler (ex. SLURM) installed on the machine you are running your code. If you have access to an HPC cluster, execute your workflow over there: ``pantarei`` will auto-detect the scheduler. Otherwise, no problem! You can install a fallback scheduler on your machine by downloading `nohupx <https://framagit.org/coslo/nohupx>`_, a minimal scheduler written in bash, and putting it somewhere in your ``$PATH``.

.. code:: bash

    wget https://framagit.org/coslo/nohupx/-/raw/master/nohupx
    mv nohupx env/bin/
    chmod u+x env/bin/nohupx

We can now wrap the task with a ``Job`` and submit jobs to the scheduler

.. code:: python

    from pantarei import Job

    job = Job(Task(square))
    for x in [4, 5, 6]:
        job(x=x)

.. warning::

    Two current limitations of pantarei ``Jobs`` are: (i) the name of Job instances must be ``job`` and (ii) all arguments to ``job`` must be passed by keyword. Hopefully, they will be removed soon.

The jobs will be running in the background. To see a summary of the jobs in the current session, execute this or add it at the end of the script.

.. code:: python

    from pantarei.core import report

    report()

In this case, the jobs were already over! You can also see the status of the jobs using the scheduler from the command line. For instance, with ``nohupx`` you can check the jobs' queue like this

.. code:: bash

    nohupx queue

Once the jobs are done, we can get the actual results.

.. code:: python

    job.scheduler.wait()
    results = job(x=6)
    print(results)

::

    36


.. note::

    Jobs and tasks share the same cache. Therefore, calling a ``Job`` that wraps a ``Task`` that has been already called will not trigger a resubmission.

Command line interface
~~~~~~~~~~~~~~~~~~~~~~

Suppose we collect the above commands in a script, say ``script.py``

.. code:: python

    from pantarei import *

    def f(x):
        import time
        time.sleep(2)
        return x

    task = Task(f)
    for x in [1, 2]:
        task(x=x)

From the command line, you can execute the script and check the state of the jobs by changing the execution mode like this

.. code:: sh

    rei run script.py  # run the full script

::

    # pantarei paths:
    .pantarei/f/216ec88e5485e1ae439c37d3f94cab8f
    .pantarei/f/4e17f7f9fc6073c3d511bc12c7b6722a


.. code:: sh

    rei summary script.py  # summary of the jobs

::

    [X] ended   |████████████████████| 100% [2/2]

    Total CPU time.......................0:00:04
    Mean CPU time per job................0:00:02
    Wall time left...........................N/A


.. code:: sh

    rei run --timid script.py  # go through the script without running the tasks/jobs

::

    # pantarei paths:
    .pantarei/f/216ec88e5485e1ae439c37d3f94cab8f
    .pantarei/f/4e17f7f9fc6073c3d511bc12c7b6722a

Workflow and data management
----------------------------

A minimal simulation workflow
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Let's build a minimal simulation workflow comprising the following steps:

- **production**: run a simulation using a ``run()`` driver, which returns some data

- **analysis**: postprocess the data and compute some stats using a ``postprocess()`` function

- **plot**: plot some graphs using the above data with some ``plot()`` function

We emulate this setup with these functions

.. code:: python

    import time
    import numpy

    def run(T=1.0, n=12, seed=1):
        # Emulate some long running simulation...
        time.sleep(2)
        # Return some synthetic thermodynamic data
        numpy.random.seed(seed)
        raw = numpy.random.normal(size=1000)
        data = {'U': 3/2 * T * raw, 'W': T + raw * n/3}
        return data

    def postprocess(data):
        results = {}
        # Compute stats
        for key in data:
            results[f'mean_{key}'] = numpy.mean(data[key])
            results[f'std_{key}'] = numpy.std(data[key])
        # Compute virial-potential energy correlation
        R = numpy.corrcoef(data['U'], data['W'])[0, 1]
        results['corr_UW'] = R
        return results

Let's turn each step into a task and run the workflow

.. code:: python

    from pprint import pprint

    data = Task(run)()
    results = Task(postprocess)(data)
    pprint(results)

::

    {'corr_UW': 1.0,
     'mean_U': 0.05821871423940277,
     'mean_W': 1.1552499046384073,
     'std_U': 1.4715062008983175,
     'std_W': 3.9240165357288466}

Browsing datasets
~~~~~~~~~~~~~~~~~

The tasks arguments and results are stored in cache in a way similar to joblib. However, pantarei provides eacy access to the **tasks' datasets** through dedicated ``Dataset`` and ``Data`` objects.

Let's first make some simulations for a range of parameters

.. code:: python

    from pantarei import Task

    run = Task(run)
    pp = Task(postprocess)

    for T in [1.0, 2.0, 3.0]:
        for seed in range(2):
            data = run(T=T, seed=seed)
            pp(data)

We can now browse all the arguments and results as a "dataset"

.. code:: python

    from pantarei.core import browse

    browse('postprocess')

::

    corr_UW data  mean_U  mean_W  std_U  std_W 
    -------------------------------------------
    1.0     {'U': 0.05821 1.15524 1.4715 3.9240
    1.0     {'U': -0.0678 0.81897 1.4805 3.9481
    1.0     {'U': -0.1357 1.81897 2.9610 3.9481
    1.0     {'U': 0.11643 2.15524 2.9430 3.9240
    1.0     {'U': -0.2036 2.81897 4.4416 3.9481
    1.0     {'U': 0.17465 3.15524 4.4145 3.9240


The returned dataset is a ``Dataset`` object with ``pandas``-like features

.. code:: python

    dataset = browse('run')
    print(dataset.columns())
    print(dataset['T'])

::

    ['T', 'U', 'W', '_dirname', '_path', 'n', 'seed']
    [1. 1. 1. 2. 2. 3. 3.]


.. note::

    You can always inspect the cache by looking inside the ``.pantarei/`` folder. Clearing the cache "manually" is fine, too.

We can create a ``Dataset`` explictly by providing paths and parsers

.. code:: python

    from pantarei import Dataset

    dataset = Dataset(parsers=[('pickle', '*.pkl')])
    dataset.insert('.pantarei/run/*/*')
    dataset

::

    T   U  W  n  seed
    -----------------
    1.0 [  [  12 1   
    2.0 [  [  12 1   
    1.0 [  [  12 0   
    2.0 [  [  12 0   
    3.0 [  [  12 0   
    1.0 [  [  12 1   
    3.0 [  [  12 1   


Actually, ``browse`` parses any files in tabular, yaml or pickle format to populate the dataset. If your simulation code store artifacts in subfolders, you can qucikly create and analyze them using Datasets!
