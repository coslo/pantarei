========
Pantarei
========

Pantarei is a general-purpose workflow manager - because *everything flows*

------------

.. toctree::
   :caption: Documentation
   :maxdepth: 2
   
   tutorial
   api/index
