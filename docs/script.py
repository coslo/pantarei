from pantarei import *

def f(x):
    import time
    time.sleep(2)
    return x

task = Task(f)
for x in [1, 2]:
    task(x=x)
